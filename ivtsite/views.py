from django.contrib import auth
from django.contrib.auth.models import User
from django.views.generic import TemplateView
from django.shortcuts import redirect, render




class HomeView(TemplateView):
	template_name = "index.html"
class ProfilePage(TemplateView):
	template_name = "registration/index.html"
class RegisterView(TemplateView):
	template_name = "registration/register.html"
	def dispatch (self, request, *args, **kwargs):
		context = {}
		if request.method == 'POST':
			username = request.POST.get("username")
			fullname = request.POST.get("fullname")
			email = request.POST.get("email")
			password = request.POST.get("password")
			password2 = request.POST.get("password2")
			if len(password)>8 and password==password2:
				user=User.objects.create_user(username, email, password)
				user.first_name=fullname
				user.save()
				message = "Регистрация прошла успешно"
			else:
				message = "Пароль больше 8 символов"
			context['message'] = message
		return render(request, self.template_name, context)
def logout(request):
	auth.logout(request)
	return redirect("/")